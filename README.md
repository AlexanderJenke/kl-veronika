# KL-Veronika

ETL für Veronikas Raucherumfrage

## files

- img/: contains generated images
- src/: contains source code
- Rauchen.csv: questionaire answers
- requirements.txt: required python packages

## run

```bash
python3 src/main.py -i Rauchen.csv 
```
