import re

import numpy as np
import pandas as pd

valid_ages = ["< 15", "15 - 17", "18 - 30", "30 - 50", "> 50"]


def age(x) -> str:
    value = x['Wie alt sind Sie?'] if x['Wie alt sind Sie?'] is not np.nan else x['Wie alt sind Sie?.1']

    if value in valid_ages:  # valid answer
        return value

    search = re.search(r'(\d+)', str(value))
    if search:
        value = search.group(0)

    try:
        value_f = int(value)  # a number was given
        if value_f < 15:
            value = "< 15"
        if 15 <= value_f <= 17:
            value = "15 - 17"
        if 18 < value_f <= 30:
            value = "18 - 30"
        if 30 < value_f <= 50:
            value = "30 - 50"
        if value_f > 50:
            value = "> 50"

        return value

    except ValueError:
        pass

    return value


def smoker(x) -> int:
    value = x["Haben Sie schonmal geraucht?"]
    if value == "Nein.":
        return 0
    elif value == "1 x, ich werde es aber definitiv nie wieder tun.":
        return 1
    elif value == "Ja.":
        return 2
    else:
        return np.nan


def cannabis(x):
    querys = ["bubatz", "marihuana", "weesl", "cannabis", "joint", "weed", "grünzeug", "gras", "broccoli",
              "kräuter", "pilz", "thc", "canabis", "lavendel"]

    value = x['Was rauchen Sie / Was haben Sie geraucht?']
    if str(value) == "nan":
        return False

    value = value.lower()

    return any([q in value for q in querys])


def start_age(x):
    search = re.search(r'(\d+)', str(x['Mit wievielen Jahren haben Sie das erste Mal geraucht?']))
    if search:
        return int(search.group(0))
    else:
        return np.nan


def reasons(x):
    value = x['Warum rauchen Sie jetzt, bzw. weshalb haben Sie geraucht?']
    reasons = set()

    if str(value) == "nan":
        return reasons

    for query, reason in zip(
            ["Sucht", "Genuss", "Stress", "Freundeskreises", "Gewohnheit", "Neugier", "Ausprobieren", "probieren"],
            ["Sucht", "Genuss", "Stress", "Freunde", "Gewohnheit", "Neugierde", "Neugierde", "Neugierde", "Neugierde"]
    ):
        if query in value:
            reasons.add(reason)

    return reasons


abh_d = {
    'Ich glaube nicht, dass das funktioniert, ansonsten würde ich schon lange nicht mehr rauchen.': 2,
    'Ich habe bereits mit dem Rauchen aufgehört.': 0,
    'dafür gibt es bei mir keinen Grund ': 1,
    'Kommt immer auf den Grund an, als ich schwanger werden wollte war es tatsächlich überhaupt kein Problem sofort aufzuhören. Durch die Arbeit und eine emotional stressige Zeit wieder angefangen und jetzt keinen Grund aufzuhören. Ich rauche fast nur auf Arbeit und niemals vor meinem Kind...bis jetzt, er ist 4': 1,
    'Ja, aber ich will nicht ': 1,
    'Ich habe bereits aufgehört, kann aber nicht sagen, dass ich nie wieder rauchen werde ': 0,
    'Vermutlich schon, würde mir vielleicht schwerfallen, aber ich könnte das durchhalten und hatte schon Rauchpausen von 1/2 Jahr etc': 1,
    'Ich finde das doof, zu sagen dass, ich nie wieder rauche. Ich könnte aber jeder Zeit aufhören.': 1,
    'Ich habe bereits einmal aufgehört und nach Jahren wieder begonnen. Ich denke das wäre temporär immer möglich. Nur möchte ich das nicht. :)': 1,
    'Ich bin kein Zigarettenraucher.': 0,
    'Könnte bestimmt, aber der Grund würde mir im Moment fehlen': 1,
    'Kein Raucher mehr': 0,
    'Weil ich sehr selten rauche und es es kein muss ist kann ich auch darauf verzichten, aber ich genieße es dennoch Mal gern': 1,
    'Könnte ich sagen und machen. Will ich aber nicht.': 1,
    'wenn ich das wirklich will, denk ich schon': 1,
    'das habe ich so mit Erfolg getan': 0,
    'Genau so war’s ': 0,
    'Ich habe nie richtig angefangen und musste daher auch nicht aufhören ': 0,
    'Könnte ich, aber ich kann nicht versprechen, das Versprechen nicht zu vergessen und ich weiß nicht, ob und warum ich vielleicht anfangen sollte, richtig zu rauchen': 1,
    'Nein': 2,
    'Habe schon mal aufgehört ': 0,
    'Ich rauche gern, muss es aber nicht.': 1,
    'Ja': 0,
    'Auf Alkohol unverzichtbar ': 1,
    'Ich könnte es, aber ich mag das Gefühl ganz gerne, wenn man nachdem man was getrunken hat raucht, will es also nicht ausschließen': 1,
    'wenn ich wirklich durchziehen wollen würde, dann ja, aber will ich nicht': 1,
    'ich könnte es mir vorstellen, jedoch möchte ich mir nichts verbieten, da es für mich ein genussmittel ist (sprich ich verzehre es nur selten)': 1,
    'Ist nicht mein Bedürfnis.': 2,
    np.nan: np.nan
}


def abhängigkeit(x):
    value = x[
        'Könnten Sie sagen: "Die nächste Zigarette, die ich rauche, ist die letzte Zigarette, die ich in meinem ganzen Leben rauchen werde!" und dies durchziehen?']
    return abh_d[value]


def passivrauchen(x):
    value = x['Wie oft bekommen Sie den Rauch von Rauchern aus Ihrer Nähe ab?']
    if value not in ["eigentlich nie",
                     'ca. 1 x pro Monat',
                     'ca. 1 x pro Woche',
                     'ca. 2-4 x pro Woche',
                     'täglich',
                     'Selten']:
        return "Sonstiges"
    return value


tmp = set()


def auswirkungen(x):
    value = x['Haben Sie köperliche Einschränkungen / Veränderungen, die Sie auf das Rauchen zurückführen würden.']

    if str(value) == 'nan':
        return "nan"

    if value in ['Keine']:
        return "Keine"

    if any([v.lower() in value.lower() for v in
            ['Kondition', 'Ausdauer', "Atem ", 'erschöpft', 'leistungsfähigkeit', 'hochgestratzt']]):
        return "Kondition/Ausdauer"

    if any([v.lower() in value.lower() for v in
            ['Asthma', "Kurzatmigkeit", 'Lungenvolumen', 'Atembeschwerden', 'Aufnahme von Sauerstoff']]):
        return "Atembeschwerden"

    if any([v in value.lower() for v in ["husten"]]):
        return "Husten"

    if any([v in value.lower() for v in ["kreislauf", "schwindel", 'blutdruck']]):
        return "Kreislaufprobleme"

    if any([v in value.lower() for v in
            ["nein", 'nö', 'nope', 'keine', 'neun', 'nicht das ich wüsste', 'ne', 'nicht']]):
        return "Keine"

    if "ja" in value.lower():
        return "Nicht genauer Benannt"

    if "krebs" in value.lower():
        return "Krebs"

    tmp.add(value)
    return "Sonstiges"


def dauer(x):
    value = x['Wie lange rauchen Sie schon bzw. haben Sie geraucht?']

    if value in ["< 1 Jahr", "1 - 2 Jahre", "2 - 5 Jahre", "6 - 9 Jahre", "10 - 14 Jahre", "15 - 20 Jahre",
                 "> 20 Jahre"]:
        return value

    return np.nan


haeufigkeit_d = {
    np.nan: np.nan,
    'nicht mehr': np.nan,
    'NichtMehr ': np.nan,
    'nur ein einziges Mal': np.nan,
    '5x / Leben': np.nan,
    'einmal': np.nan,
    'Insgesamt vielleicht 10 Mal ': np.nan,
    'jetzt Bichtraucher': np.nan,
    '1x 1 Zigarette... wie oft noch?': np.nan,
    'Ganz selten als Jugendlicher': np.nan,
    'Einmal ': np.nan,
    ' Nie regelmäßig, letztmalig mit 24 Jahren': np.nan,

    'Sehr unregelmäßig, meist auf Feiern oder bei Treffen mit Freunden, hin und wieder auch nach einer langen Lerneinheit': "nur zu besonderen Anlässen",
    'nur zu besonderen Anlässen und dann auch nur, wenn man Lust hat.': "nur zu besonderen Anlässen",
    'nur zu besonderen Anlässen': "nur zu besonderen Anlässen",
    'Partyraucher, aber dann immer so 5-10': "nur zu besonderen Anlässen",
    'Wenn ich besoffen bin rauche ich gerne': "nur zu besonderen Anlässen",
    'einmal pro jahr ca.': "nur zu besonderen Anlässen",

    '1 x pro Monat': "1 x pro Monat",
    'Aller 2 Wochen': "1 x pro Monat",

    '1 x pro Woche': "1 x pro Woche",
    '3 x die Woche ': "1 x pro Woche",
    '2,3 mal in der Woche ': "1 x pro Woche",
    '2-4 x pro Woche': "1 x pro Woche",
    'ca. 2x pro woche, dann aber mind. 4 Zigaretten ': "1 x pro Woche",
    '2-3 mal pro Woche ': "1 x pro Woche",
    '2x pro Woche ': "1 x pro Woche",
    'Mehrmals die woche, aber unregelmäßig und eher wenig (weniger als 5 insgesamt)': "1 x pro Woche",

    '1 x pro Tag': "1 x pro Tag",

    '2-4 x am Tag': "2-4 x am Tag",

    'mehr als 4 x am Tag': "mehr als 4 x am Tag",
    'Heute ca. 5-10, als Jugendliche ca. 25 am Tag auch mal mehr': "mehr als 4 x am Tag",

    'ca. 1 Packung (a 20 Stk.) am Tag': "ca. 1 Packung (a 20 Stk.) am Tag",

    '> 1 Packung (a 20 Stk.) am Tag': "> 1 Packung (a 20 Stk.) am Tag",
}


def haeufigkeit(x):
    value = x['Wie oft rauchen Sie / haben Sie geraucht?']
    return haeufigkeit_d[value]


def etl(csv_path: str) -> pd.DataFrame:
    df_csv = pd.read_csv(csv_path)
    df = pd.DataFrame()

    # ETL
    df['Time'] = df_csv.Zeitstempel
    df['Age'] = df_csv.apply(lambda x: age(x), axis=1)
    df['Smoker'] = df_csv.apply(lambda x: smoker(x), axis=1)
    df['Cannabis'] = df_csv.apply(lambda x: cannabis(x), axis=1)
    df['StartAge'] = df_csv.apply(lambda x: start_age(x), axis=1)
    df['Reasons'] = df_csv.apply(lambda x: reasons(x), axis=1)
    df['Abhängigkeit'] = df_csv.apply(lambda x: abhängigkeit(x), axis=1)
    df['PassivRauchen'] = df_csv.apply(lambda x: passivrauchen(x), axis=1)
    df['Auswirkungen'] = df_csv.apply(lambda x: auswirkungen(x), axis=1)
    df['Dauer'] = df_csv.apply(lambda x: dauer(x), axis=1)
    df['Häufigkeit'] = df_csv.apply(lambda x: haeufigkeit(x), axis=1)

    return df


if __name__ == '__main__':
    df = etl("../Rauchen.csv")
    print()
