from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

import etl


def plot_all(df: pd.DataFrame):
    for plot in tqdm([
        smoker_per_age,
        cannabis_per_age,
        start_age,
        reasons,
        reasons_stacked,
        cumsum_time,
        abhängigkeit_per_age,
        passivrauchen,
        auswirkungen,
        auswirkungen_details,
        auswirkungen_per_dauer,
        häufigkeit_pro_grund,
        anteil_häufigkeit_pro_grund
    ], desc="Plot"):
        plot(df)


def cumsum_time(df):
    plt.figure("CumsumTime")
    dates = np.asarray(df.Time)

    x = []
    for date in dates:
        date, time, ap, zone = date.split(" ")
        hour, minute, second = time.split(":")
        hour = int(hour) % 12
        if ap == "PM":
            hour += 12
        year, month, day = date.split("/")
        x.append(
            datetime(year=int(year), month=int(month), day=int(day), hour=hour % 24, minute=int(minute),
                     second=int(second))
        )
    y = np.arange(0, len(x))
    plt.title("Umfragebeteiligung")
    plt.ylabel("Antworten")
    plt.xlabel("Zeit")
    plt.xticks(rotation=45, ha='right')
    plt.plot(x, y)
    plt.tight_layout()
    plt.savefig("../img/Umfragebeteiligung.png")
    plt.close()


def reasons(df):
    fig, ax = plt.subplots()
    df_h = pd.DataFrame()
    for _, row in df.iterrows():
        for reason in row.Reasons:
            df_h = df_h.append({"Alter": row.Age, "Gründe": reason}, ignore_index=True)

    sns.countplot(
        ax=ax,
        data=df_h,
        x="Alter",
        hue="Gründe",
        order=etl.valid_ages,
    )

    plt.title("Warum Rauchen Sie?")
    plt.ylabel("Anzahl")
    plt.tight_layout()
    plt.savefig("../img/Reasons.png")
    plt.close()


def reasons_stacked(df):
    df_h = pd.DataFrame()
    for _, row in df.iterrows():
        for reason in row.Reasons:
            df_h = df_h.append({"Alter": row.Age, "Gründe": reason}, ignore_index=True)

    reasons = ["Sucht", "Genuss", "Stress", "Freunde", "Gewohnheit", "Neugierde"]

    plt.figure("ReasonsStacked")
    data = np.zeros((6, 5))
    for i, reason in enumerate(reasons):
        values = np.asarray(df_h[df_h.Gründe == reason].Alter)
        data[i] = [sum(values == v) for v in etl.valid_ages]

    n = data.sum(axis=0)
    data = data / n * 100
    x = [f"{name}\n(N={int(count)})" for name, count in zip(etl.valid_ages, n)]

    for i in range(6):
        plt.bar(x=x, height=data[i], bottom=data[:i].sum(axis=0), label=reasons[i], width=.5)

    plt.title("Warum Rauchen Sie?")
    plt.xlabel("Altersgruppe")
    plt.ylabel("Anteil (in %)")
    plt.legend(title="Gründe", bbox_to_anchor=(1, 1))
    plt.tight_layout()
    plt.savefig("../img/ReasonsStacked.png")
    plt.close()


def start_age(df):
    plt.figure("StartAlter")
    data = np.asarray(df[df.Smoker > 0].StartAge)
    data = np.nan_to_num(data)

    x = sorted(set(data))
    if x[0] == 0:
        x = x[1:]
    y = [sum(data == v) for v in x]

    plt.bar(x, y)
    plt.title("Mit wievielen Jahren haben Sie das erste Mal geraucht?")
    plt.xlabel("Alter")
    plt.ylabel("Anzahl")
    plt.tight_layout()
    plt.savefig("../img/StartAlter.png")
    plt.close()


def smoker_per_age(df):
    plt.figure("Altersverteilung")
    data = np.zeros((3, 5))
    for smoker in range(3):
        values = np.asarray(df[df.Smoker == smoker].Age)
        data[smoker] = [sum(values == v) for v in etl.valid_ages]

    n = data.sum(axis=0)
    data = data / n * 100
    x = [f"{name}\n(N={int(count)})" for name, count in zip(etl.valid_ages, n)]
    label = ["Nein", "1x probiert", "Ja"]
    # color = ["xkcd:green", "xkcd:orange", "xkcd:blue"]
    # color = ["tab:green", "tab:orange", "tab:blue"]
    color = sns.color_palette('pastel')

    for i in range(3):
        plt.bar(x=x, height=data[i], bottom=data[:i].sum(axis=0), label=label[i], color=color[i], width=.5)

    plt.title("Altersverteilung")
    plt.xlabel("Altersgruppe")
    plt.ylabel("Anteil (in %)")
    plt.legend(title="Rauchen Sie?", bbox_to_anchor=(1, 1))
    plt.tight_layout()
    plt.savefig("../img/Altersverteilung.png")
    plt.close()


def cannabis_per_age(df):
    plt.figure("Cannabis")
    data = df[df.Smoker > 0][["Age", "Cannabis"]]
    proportion = np.asarray([df[(df.Age == age) & (df.Cannabis == True)].shape[0] / df[df.Age == age].shape[0]
                             for age in etl.valid_ages])
    proportion *= 100
    plt.bar(x=etl.valid_ages, height=proportion, width=.5)
    plt.title("Anteil an Rauchern, die angeben auch Cannabis zu konsumieren")
    plt.xlabel("Altersgruppe")
    plt.ylabel("Anteil (in %)")
    plt.tight_layout()
    plt.savefig("../img/Cannabis.png")
    plt.close()


def abhängigkeit_per_age(df):
    data = df[['Age', 'Abhängigkeit']]
    data = data[data.Abhängigkeit.notna()]
    data = data.rename(columns={'Age': 'Alter'})

    sns.countplot(
        data=data,
        x="Alter",
        hue="Abhängigkeit",
        order=etl.valid_ages,

    )
    plt.ylim(0, 100)
    plt.title("Wie Abhänging sind Sie?")
    plt.ylabel("Anzahl")
    plt.tight_layout()
    plt.savefig("../img/Abhängigkeit.png")
    plt.close()


def passivrauchen(df):
    data = df[df.Smoker < 2][['PassivRauchen']]

    counts = {v: len(data[data.PassivRauchen == v]) for v in ["eigentlich nie",
                                                              'ca. 1 x pro Monat',
                                                              'ca. 1 x pro Woche',
                                                              'ca. 2-4 x pro Woche',
                                                              'täglich',
                                                              'Selten', "Sonstiges"]}

    keys = ["eigentlich nie",
            'Selten',
            'ca. 1 x pro Monat',
            'ca. 1 x pro Woche',
            'ca. 2-4 x pro Woche',
            'täglich',
            "Sonstiges"]
    colors = sns.color_palette('pastel')
    plt.pie([counts[k] for k in keys], colors=colors, labels=keys, autopct='%0.0f%%')
    plt.title("Wie oft sind Sie Passivrauch ausgesetzt?")
    plt.tight_layout()
    plt.savefig("../img/Passivrauchen.png")
    plt.close()


def auswirkungen(df):
    data = df[df.Smoker > 0][['Auswirkungen']]
    data = data[data.Auswirkungen != "nan"]

    keys = set(data.Auswirkungen)
    counts = {v: len(data[data.Auswirkungen == v]) for v in keys}

    colors = sns.color_palette('pastel')
    ordered_keys = sorted(keys, key= lambda x:counts[x])
    plt.pie([counts[k] for k in ordered_keys], colors=colors, labels=ordered_keys, autopct=lambda x: f"{x:.1f}%" if x > 10 else "")
    plt.title("Welche Auswirkungen hat das Rauchen auf Sie?")
    plt.tight_layout()
    plt.savefig("../img/Auswirkungen.png")
    plt.close()


def auswirkungen_details(df):
    data = df[df.Smoker > 0][['Auswirkungen']]
    data = data[data.Auswirkungen != "nan"]
    data = data[data.Auswirkungen != "Keine"]

    keys = set(data.Auswirkungen)
    counts = {v: len(data[data.Auswirkungen == v]) for v in keys}

    colors = sns.color_palette('pastel')
    ordered_keys = sorted(keys, key= lambda x:counts[x])
    plt.pie([counts[k] for k in ordered_keys], colors=colors, labels=ordered_keys, autopct='%0.0f%%')
    plt.title("Welche Auswirkungen hat das Rauchen auf Sie?")
    plt.tight_layout()
    plt.savefig("../img/AuswirkungenDetail.png")
    plt.close()


def auswirkungen_per_dauer(df):
    data = df[df.Smoker > 0][['Dauer', 'Auswirkungen']]
    data = data[data.Auswirkungen != "nan"]
    data = data[data.Auswirkungen != np.nan]
    data = data[data.Auswirkungen.notna()]
    data = data[data.Auswirkungen != "Keine"]
    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    sns.countplot(
        ax=ax,
        data=data,
        x="Dauer",
        hue="Auswirkungen",
        order=["< 1 Jahr", "1 - 2 Jahre", "2 - 5 Jahre", "6 - 9 Jahre", "10 - 14 Jahre", "15 - 20 Jahre", "> 20 Jahre"],
    )
    plt.title("Welche Auswirkungen hat das Rauchen auf Sie?")
    plt.ylabel("Anzahl")
    sns.move_legend(ax, "upper right")
    plt.tight_layout()
    plt.savefig("../img/AuswirkungenPerDauer.png")
    plt.close()


def häufigkeit_pro_grund(df):
    data = df[df.Smoker > 0][['Häufigkeit', 'Reasons']]

    df_t = pd.DataFrame(columns=['Häufigkeit', 'Grund'])
    for _, row in data.iterrows():
        for reason in row.Reasons:
            df_t = df_t.append({'Häufigkeit': row['Häufigkeit'], 'Grund': reason}, ignore_index=True)

    hue_order = [
        "nur zu besonderen Anlässen",
        "1 x pro Monat",
        "1 x pro Woche",
        "1 x pro Tag",
        "2-4 x am Tag",
        "mehr als 4 x am Tag",
        "ca. 1 Packung (a 20 Stk.) am Tag",
        "> 1 Packung (a 20 Stk.) am Tag",
    ]

    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    sns.countplot(
        ax=ax,
        data=df_t,
        x="Grund",
        hue="Häufigkeit",
        hue_order=hue_order,
        order=['Neugierde', 'Freunde', 'Genuss', 'Stress', 'Gewohnheit', 'Sucht']
    )
    plt.title('Wie oft rauchen Sie / haben Sie geraucht?')
    plt.ylabel("Anzahl")
    sns.move_legend(ax, "upper right")
    plt.tight_layout()
    plt.savefig("../img/HäufigkeitPerGrund.png")
    plt.close()


def anteil_häufigkeit_pro_grund(df):
    data = df[df.Smoker > 0][['Häufigkeit', 'Reasons']]
    data = data[data['Häufigkeit'] != np.nan]

    df_t = pd.DataFrame(columns=['Häufigkeit', 'Grund'])
    for _, row in data.iterrows():
        for reason in row.Reasons:
            df_t = df_t.append({'Häufigkeit': row['Häufigkeit'], 'Grund': reason}, ignore_index=True)

    df_c = pd.DataFrame(columns=['Anteil', 'Grund', 'Häufigkeit'])
    for reason in pd.unique(df_t.Grund):
        df_r = df_t[df_t.Grund == reason]
        df_r = df_r[df_r['Häufigkeit'].notnull()]
        for amount in pd.unique(df_t['Häufigkeit']):
            df_c = df_c.append({
                'Häufigkeit': amount,
                'Anteil': len(df_r[df_r['Häufigkeit'] == amount]) / len(df_r) * 100,
                'Grund': reason,
            }, ignore_index=True)

    hue_order = [
        "nur zu besonderen Anlässen",
        "1 x pro Monat",
        "1 x pro Woche",
        "1 x pro Tag",
        "2-4 x am Tag",
        "mehr als 4 x am Tag",
        "ca. 1 Packung (a 20 Stk.) am Tag",
        "> 1 Packung (a 20 Stk.) am Tag",
    ]
    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    sns.barplot(
        ax=ax,
        data=df_c,
        x="Grund",
        y="Anteil",
        hue="Häufigkeit",
        hue_order=hue_order,
        order=['Neugierde', 'Freunde', 'Genuss', 'Stress', 'Gewohnheit', 'Sucht']
    )
    plt.title('Wie oft rauchen Sie / haben Sie geraucht?')
    plt.ylabel("Anteil (in %)")
    sns.move_legend(ax, "upper center")
    plt.tight_layout()
    plt.savefig("../img/AnteilHäufigkeitPerGrund.png")
    plt.close()
