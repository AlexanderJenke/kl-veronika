from argparse import ArgumentParser

import seaborn as sns

from etl import etl
from plots import plot_all

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", help="csv file with data")
    args = parser.parse_args()

    df = etl(args.input)

    sns.set_theme(style="whitegrid")
    sns.set_palette('pastel')
    plot_all(df)
